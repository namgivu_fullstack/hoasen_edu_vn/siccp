a = 1
b = 22
# print(a) ; print(b)
print(f'a={a} b={b}')
# a=1 b=22

b_old = b
b = a
a = b_old
print(f'a={a} b={b}')
