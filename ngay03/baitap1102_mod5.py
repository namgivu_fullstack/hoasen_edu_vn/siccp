"""
Receive one integer.
If the integer is a multiple of 5, return True.
                          If not, return False.
"""
n = 125

# multiple of 5 ie mod 5 == 0
r = n % 5
if r == 0:
    print(True)
else:
    print(False)

# multiple of 5 ie last digit is 0 or 5
s = '125'
last_digit = s[-1]

if last_digit == '0'  or  last_digit == '5':
    print(True)
else:
    print(False)

i=122
