"""
Receive a 3-digit integer from the user.
If the hundredth digit of the integer n is 3, return True.
                                      If not, return False.
"""
s = '323'
hundredth_digit = s[0]

if hundredth_digit == '3':
    print(True)
else:
    print(False)
