def lonnhat(d):
    # revenue_list = [ int(o['revenue'].replace('$','').replace(',','')) for o in d]
    revenue_list = [ o['revenue']                       for o in d]
    revenue_list = [ o.replace('$','').replace(',','')  for o in revenue_list]
    revenue_list = [ int(o)                             for o in revenue_list ]

    max_revenue = max(revenue_list)
    return max_revenue

import json
with open('mcdonald_revenue.json','r') as f:
    d = json.load(f)
    d = lonnhat(d)
