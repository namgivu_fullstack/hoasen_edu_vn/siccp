s = 'year' ; print(s)

y = '2023'
s = 'year' ;        print(s, y)
s = 'year ' + y ;   print(s)

y = '2222'
r = '$333000'
s = 'year ' + y + ' revenue ' + r;   print(s)
s = f'year {y} revenue {r}';   print(s)
s = f'year {y}\nrevenue {r}';   print(s)

s='a\nb'
print(s)

s='''
a
b
'''
print(s)


s = f'year {y}\nrevenue {r}';   print(s)
s = f'''
year {y} 
revenue {r}
'''
print(s)
