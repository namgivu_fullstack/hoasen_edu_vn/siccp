import random

training_data = [
    #'query                                                         'a ie answer': 'xx'       },
    {'q': ['hi', 'hello', 'good morning'],                          'a': ['How are you?', 'Hi', 'Hi there!']     },
    {'q': ['bye', 'good bye', 'tam biet', 'bye bye'],               'a': ['See you again', 'Bye' ]               },
    {'q': ['chao', 'chào', 'xin chao'],                             'a': ['Bạn khỏe không?']  },
    #
    {'q': ['cha đẻ của bạn là ai','ai là người tạo ra bạn'],        'a': ['SICCP của HSU' ] },
    {'q': ['1+1 bằng mấy','một cộng một bằng mấy','1+1=','1+1' ],   'a': ['1+1=2'] },  #TODO tongquat 1+1 bang bieuthuc batky
    {'q': ['Học phí Hoa Sen 2023'],                                 'a': ['100 triệu'] },
    {'q': ['Học phí Hoa Sen 2022'],                                 'a': ['50 triệu']  },
    {'q': ['toi khoe con ban',          'tôi khỏe còn bạn',
           'toi cung khoe con ban',     'tôi cũng khỏe còn bạn',
           'toi khoe cam on con ban',   'tôi khỏe cảm ơn còn bạn',
           #
           'tôi khỏe',              'toi khoe',
           'toi khoe cam on',       'tôi khỏe cảm ơn',
           'toi cung khoe',         'tôi cũng khỏe',
           'toi cung khoe cam on',  'tôi cũng khỏe cảm ơn'
           ],
     'a': ['tôi cũng khỏe cảm ơn! bạn cần tôi giúp gì không?', 'bạn cần giúp gì không?'],
     },

    {'q': ['Hoa Sen đang có cuộc thi gì không?', 'cuộc thi'], 'a': ['Hoa Sen đang có cuộc thi IBA']},

    {'q': ['Blockchain là gì?', 'Bạn có thể giải thích ngắn gọn về Blockchain không?', 'Blockchain?'],
     'a': ['Blockchain là một công nghệ lưu trữ và truyền tải thông tin một cách an toàn và minh bạch']},
    {'q': ['Blockchain được dùng để làm gì?', 'Ứng dụng của Blockchain'],
     'a': ['Các ứng dụng của Blockchain rất đa dạng, từ lưu trữ tài sản kỹ thuật số, đến quản lý chuỗi cung ứng, bảo hiểm, thực hiện giao dịch tài chính, bỏ phiếu điện tử, v.v.']},
]

def get_answer(q):
    """
    tim :q trong :training_data, va trave :a tuong ung
    """
    found = None
    for i in training_data:
        i_at_q = [s.lower().strip(' !?.,') for s in i['q'] ]

        if q in i_at_q:  # khi i['q'] la 1 mang
            found = i['a']
            break

    #rergion random choose 1 answer from answer list in :found
    if found:
        if not isinstance(found, list): found = [found]  # convert ans. to list from single str value

        a = found[ random.randint(0,len(found)-1) ]
    else:
        a = None
    #endrergion random choose 1 answer from answer list in :found

    return a

while True:
    q = input('> ').lower().strip(' !?.,')

    if q in ['quit', 'exit', ':q']:
        break

    answer = get_answer(q)
    answer = '.' if not answer else answer
    print(answer)
