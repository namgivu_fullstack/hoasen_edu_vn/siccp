import json

print(
    json.dumps(
        {'a': 2, 'b': {'x': 3, 'y': {'t1': 4, 't2': 5}}},
        sort_keys=True, indent=4
    )
)

print(
    json.dumps(
        [
            {'a': 122},
            {'a': 122},
        ]
        ,
        sort_keys=True, indent=4
    )
)
