training_data = [
    #'query                 'a ie answer': 'xx'    },
    {'q': 'hi',             'a': 'How are you?'    },
    {'q': 'hello',          'a': 'How are you?'    },  #TODO put :hello and :hi into list at key :q
    {'q': 'chao',           'a': 'Bạn khỏe không?' },
]

while True:
    q = input('> ').strip(' !?').lower()

    if q in [':q', 'exit']:
        break

    #region verbose
    # found = False
    # for d in training_data:
    #     if d['q'] == q:
    #         print(d['a'])
    #         found = True
    #         break
    #
    # if found == False:
    #     print('.')
    #endregion verbose

    #region lite code
    seek_list = [d for d in training_data  if d['q'] == q ]
    if seek_list:
        print(seek_list)
        print(seek_list[0]['a'])
    else:
        print('.')
    #endregion lite code
