d_blank = {
}

d_sample = {
    'key' : 'value',
    122   : 333,
    '455' : 333,
    666   : 788,
}

d_mcdonald = {
    '2022': '$100,000',
    '2021': '$800,000',
}

import json
with open(r'output\data.json', 'w') as f:
    json.dump(d_sample, f)
