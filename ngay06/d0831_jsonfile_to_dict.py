import json

with open(r'output/data.json', 'r') as f:
    d = json.load(f)

print(d)
i = list(d)[1]

print(d[0] ['year'])
print(d[1] ['year'])
print(d[2] ['year'])
print(d[3] ['year'])
print(d[4] ['year'])
print(d[5] ['year'])
print(d[6] ['year'])
print(d[7] ['year'])
print(d[8] ['year'])
print(d[9] ['year'])
print(d[10]['year'])
print(d[11]['year'])
print(d[12]['year'])

n = len(d)
print(d[n-1] ['year'])
print(d[n-1] ['revenue'])
print(d[n-1] ['year'], ':', d[n-1] ['revenue'])

n = len(d)
for i in range(n):
    print(d[i] ['year'], ':', d[i] ['revenue'])

for i in d:
    print(i['year'], ':', i['revenue'])
